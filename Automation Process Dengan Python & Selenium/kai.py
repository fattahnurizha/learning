from selenium import webdriver
from selenium.webdriver.support.select import Select
import time

chrome_options = webdriver.ChromeOptions()

#inisiasi chrome driver
driver=webdriver.Chrome('chromedriver.exe')
driver.maximize_window()

#buka browser
driver.get('https://www.kai.id/')

awal=driver.find_element_by_xpath('//*[@id="select2-origination2-container"]')
awal.click()
time.sleep(1)

pilih_awal=driver.find_element_by_xpath('/html/body/span/span/span[1]/input')
pilih_awal.click()
pilih_awal.send_keys('Gambir')
time.sleep(1)

klik_awal=driver.find_element_by_xpath('//*[@id="select2-origination2-results"]/li/ul')
klik_awal.click()
time.sleep(1)

tujuan=driver.find_element_by_xpath('//*[@id="select2-destination2-container"]')
tujuan.click()
time.sleep(1)

pilih_tujuan=driver.find_element_by_xpath('/html/body/span/span/span[1]/input')
pilih_tujuan.click()
pilih_tujuan.send_keys('Bandung')
time.sleep(1)

klik_tujuan=driver.find_element_by_xpath('//*[@id="select2-destination2-results"]/li[1]/ul')
klik_tujuan.click()
time.sleep(1)

tgl=driver.find_element_by_xpath('//*[@id="departure_dateh2"]')
tgl.click()
time.sleep(1)

pilih_tgl=driver.find_element_by_xpath('//*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[5]')
pilih_tgl.click()
time.sleep(1)

penumpang=driver.find_element_by_xpath('//*[@id="bookform"]/div[4]/span/span[1]/span')
penumpang.click()
time.sleep(1)

pilih_penumpang=driver.find_element_by_xpath('/html/body/span/span/span[1]/input')
pilih_penumpang.click()
pilih_penumpang.send_keys('2')
time.sleep(1)

klik_penumpang=driver.find_element_by_xpath('//*[@id="select2-adult2-results"]/li/ul')
klik_penumpang.click()
time.sleep(1)

proses=driver.find_element_by_xpath('//*[@id="submittrain"]')
proses.click()
time.sleep(1)

pesan=driver.find_element_by_xpath('//*[@id="data0"]/a/div/div[3]/div/div[2]/div')
pesan.click()
time.sleep(1)

tanpa_daftar=driver.find_element_by_xpath('//*[@id="login"]/div[3]/button')
tanpa_daftar.click()
time.sleep(1)

no_hp=driver.find_element_by_name('pemesan_nohp')
no_hp.send_keys('0812312345')
time.sleep(1)

nama=driver.find_element_by_name('pemesan_nama')
nama.send_keys('Adi Cs')
time.sleep(1)

identitas=driver.find_element_by_name('pemesan_notandapengenal')
identitas.send_keys('1234567289876526')
time.sleep(1)

alamat=driver.find_element_by_name('pemesan_alamat')
alamat.send_keys('Cikarang')
time.sleep(1)

email=driver.find_element_by_name('pemesan_email')
email.send_keys('a@gmail.com')
time.sleep(1)

check=driver.find_element_by_name('copy')
check.click()
time.sleep(1)

select=Select(driver.find_element_by_id('penumpang_title2'))
time.sleep(1)

select.select_by_visible_text('Nyonya')
time.sleep(1)

nama2=driver.find_element_by_id('penumpang_nama2')
nama2.send_keys('ira')
time.sleep(1)

ktp2=driver.find_element_by_id('penumpang_notandapengenal2')
ktp2.send_keys('1098275647382912')
time.sleep(1)

klik=driver.find_element_by_xpath('//*[@id="setuju"]')
klik.click()
time.sleep(10)

driver.close()


