from selenium import webdriver
import pandas as pd

driver=webdriver.Chrome('chromedriver.exe')
driver.get('https://en.wikipedia.org/wiki/List_of_countries_by_number_of_Internet_users')
driver.maximize_window()

country=driver.find_elements_by_xpath('//table[@class="wikitable sortable jquery-tablesorter"]//tbody/tr/td[1]')
region=driver.find_elements_by_xpath('//table[@class="wikitable sortable jquery-tablesorter"]//tbody/tr/td[3]')
internetusers=driver.find_elements_by_xpath('//table[@class="wikitable sortable jquery-tablesorter"]//tbody/tr/td[4]')
percent=driver.find_elements_by_xpath('//table[@class="wikitable sortable jquery-tablesorter"]//tbody/tr/td[5]')
population=driver.find_elements_by_xpath('//table[@class="wikitable sortable jquery-tablesorter"]//tbody/tr/td[6]')
year=driver.find_elements_by_xpath('//table[@class="wikitable sortable jquery-tablesorter"]//tbody/tr/td[8]')

pengguna_internet=[]
for i in range(len(country)):
    data={'Country': country[i].text,
    'Region': region[i].text,
    'Internet Users': internetusers[i].text,
    'Percents': percent[i].text,
    'Population': population[i].text,
    'Year': year[i].text}
    pengguna_internet.append(data)

dataframe=pd.DataFrame(pengguna_internet)
dataframe.to_excel('Pengguna Internet.xlsx', index=False)
